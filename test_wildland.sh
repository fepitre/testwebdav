#!/bin/bash

### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
### !!! DO NOT EXECUTE THIS SCRIPT ON YOUR WILDLAND SETUP !!!
### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

set -xe

wl stop || true
rm -rf /home/user/.config/wildland /home/user/.config/wildland-alice /home/user/.config/wildland-bob ~/Downloads/alice.user.yaml ~/Downloads/bob.user.yaml

sleep 1

switch_user() {
    new_user="$1"
    current_user=$(wl user dump @default-owner | grep /users/ | cut -f 3 -d /)
    if [ "$new_user" = "$current_user" ]; then
        echo "Already at $new_user" >&2
        return
    fi
    if [ -d ~/.config/wildland-$current_user ]; then
        echo "Something went wrong, ~/.config/wildland-$current_user already exists" >&2
        exit 1
    fi
    wl stop 2>/dev/null || :
    mv -f ~/.config/wildland ~/.config/wildland-$current_user
    if [ -d ~/.config/wildland-$new_user ]; then
        mv -f ~/.config/wildland-$new_user ~/.config/wildland
    else
        mkdir -p ~/.config/wildland
    fi
    echo "Switched user to $new_user"
}

wl user create bob
mkdir -p ~/Downloads/
cp ~/.config/wildland/users/bob.user.yaml ~/Downloads

switch_user alice
wl user create alice
wl user import ~/Downloads/bob.user.yaml

wl container create alice-and-bob --access bob --path /very/secret
wl storage create dummy --container alice-and-bob

wl template create webdav myshare \
    --login 'user' \
    --password 'password' \
    --url 'http://localhost:8081/'

wl forest create --access bob --owner alice myshare

wl container publish alice-and-bob
cp ~/.config/wildland/users/alice.user.yaml ~/Downloads

switch_user bob
wl user import --path /forests/alice ~/Downloads/alice.user.yaml

wl start

wl container mount :/forests/alice:/very/secret:
